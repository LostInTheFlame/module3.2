﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            string input = "25";
            int result = 0;
            int n = 5;
            Task4 task4 = new Task4();
            bool resultTPNN = task4.TryParseNaturalNumber(input, out result);
            int[] resultFibonacci = task4.GetFibonacciSequence(n);
            Console.WriteLine(resultTPNN);
            foreach (int i in resultFibonacci)
            {
                Console.WriteLine(i);
            }

            int sourceNumber = 897;
            Task5 task5 = new Task5();
            int resultReverse = task5.ReverseNumber(sourceNumber);
            Console.WriteLine(resultReverse);

            int size = 5;
            int[] source = new int[6] { -10, -11, -12, -4, -5, -1};
            Task6 task6 = new Task6();
            int[] resultArray = task6.GenerateArray(size);
            int[] resultToOpposite = task6.UpdateElementToOppositeSign(source);
            foreach (int i in resultArray)
            {
                Console.WriteLine(i);
            }
            foreach (int i in resultToOpposite)
            {
                Console.WriteLine(i);
            }

            Task7 task7 = new Task7();
            List<int> resultFindElements = task7.FindElementGreaterThenPrevious(source);
            foreach (int i in resultFindElements)
            {
                Console.WriteLine(i);
            }

            int sizeTask8 = 5;
            Task8 task8 = new Task8();
            int[,] resultTask8 = task8.FillArrayInSpiral(sizeTask8);
            for (int i = 0; i < sizeTask8; i++)
            {
                for (int j = 0; j < sizeTask8; j++)
                {
                    Console.Write($"{resultTask8[i, j]} \t");
                }
                Console.WriteLine();
            }

        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (Int32.TryParse(input, out result) && result >= 0)
            {
                return true;
            }
            return false;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] arrayFibonacci = new int[n];
            if (n == 1)
            {
                arrayFibonacci[0] = 0;
            }

            if (n == 2)
            {
                arrayFibonacci[0] = 0;
                arrayFibonacci[1] = 1;
            }

            if (n > 2)
            {
                arrayFibonacci[0] = 0;
                arrayFibonacci[1] = 1;

                for (int i = 2; i < n; i++)
                {
                    arrayFibonacci[i] = arrayFibonacci[i - 2] + arrayFibonacci[i - 1];
                }
            }
            return arrayFibonacci;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int result = 1;
            if (sourceNumber < 0)
            {
                sourceNumber *= -1;
                result *= -1;
            }
            char[] arrayReverse = sourceNumber.ToString().ToCharArray();
            Array.Reverse(arrayReverse);
            return result * Convert.ToInt32(string.Join("", arrayReverse));
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] generateArr;

            if (size < 0)
            {
                generateArr = new int[0];
                return generateArr;
            }

            Random rand = new Random();
            generateArr = new int[size];

            for (int i = 0; i < generateArr.Length; i++)
            {
                int randValue = rand.Next(0, 100);
                generateArr[i] = randValue;
            }
            return generateArr;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] *= -1;
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] generateArr;

            if (size < 0)
            {
                generateArr = new int[0];
                return generateArr;
            }

            Random rand = new Random();
            generateArr = new int[size];

            for (int i = 0; i < generateArr.Length; i++)
            {
                int randValue = rand.Next(0, 100);
                generateArr[i] = randValue;
            }
            return generateArr;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> findElements = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    findElements.Add(source[i]);
                }
            }
            return findElements;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int parsedRow = size;
            int parsedColumn = size;
            int[,] array = new int[parsedRow, parsedColumn];
            int[] elementsArray = new int[parsedRow * parsedColumn];

            for (int i = 0; i < elementsArray.Length; i++)
            {
                elementsArray[i] = i + 1;
            }

            int m = 0;
            int checkElements = parsedRow * parsedColumn - 1;

            for (int count = 0, rowInArray = parsedRow - 1, columnInArray = parsedColumn - 1; m <= checkElements; count++, rowInArray--, columnInArray--)
            {
                for (int i = count, j = count; j <= columnInArray && m <= checkElements; j++)
                {
                    array[i, j] = elementsArray[m];
                    m++;
                }

                for (int i = count + 1, j = columnInArray; i <= rowInArray - 1 && m <= checkElements; i++)
                {
                    array[i, j] = elementsArray[m];
                    m++;
                }

                for (int i = rowInArray, j = columnInArray; j >= count && m <= checkElements; j--)
                {
                    array[i, j] = elementsArray[m];
                    m++;
                }

                for (int i = rowInArray - 1, j = count; i >= count + 1 && m <= checkElements; i--)
                {
                    array[i, j] = elementsArray[m];
                    m++;
                }
            }

            return array;
        }
    }
}
